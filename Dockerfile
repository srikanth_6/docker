FROM cirruslink/ignition-edge-linux-64:latest
RUN apk add zip && apk add apk-cron && apk add npm
RUN touch start
RUN sed '2,1s/^/crond/' /root/startup.sh > /start
RUN chmod +x /start

